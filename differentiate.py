# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 14:33:08 2019

@author: xd800637
"""

# Function for calculating pressure, pressure gradients, our numerical estimate
# of geothermal wind, and an analytic calculation of the geothermal wind. For
# N + 1 evenly spaced locations and output into one array. Also a function for
# calculating the errors between the numerical and the analytical solutions.


import numpy as np
import geoParameters as gp


# Function to calculate delta(y).
def dta(N):
    '''Calculates the value of delta(y) for use in geoWind_array(N)'''
    # Value checks (will also be done in geoWind_array())
    assert N != 0, 'N must be non-zero.'

    return (gp.ymax - gp.ymin)/N


def geoWind_array(N):
    '''Numerically evaluates the wind at N + 1 evenly spaced locations, starting
    at y_0 = ymin and ending at y_N = ymax, where ymin and ymax are defined in
    geoParameters.py.'''

    # Value checks
    assert (type(N) == int and N > 0), 'N must be a positive integer.'

    # Initialize array
    gwarray = np.zeros(shape=(N + 1, 5))

    # Shortcut for dta(N)
    d = dta(N)

    # Populate with y_n values
    for i in range(N + 1):
        gwarray[i, 0] = gp.ymin + i*d

    # populate with pressure values
    for i in range(N + 1):
        gwarray[i, 1] = gp.pressure(gwarray[i, 0])

    # populate with pressure gradient values. Note different estimation methods
    # for end points.
    gwarray[0, 2] = (gwarray[1, 1] - gwarray[0, 1])/d
    gwarray[N, 2] = (gwarray[N, 1] - gwarray[N - 1, 1])/d

    for i in range(1, N):
        gwarray[i, 2] = (gwarray[i + 1, 1] - gwarray[i - 1, 1])/(2 * d)

    # populate with geostrophic wind values (calculated numerically)
    for i in range(N + 1):
        gwarray[i, 3] = gp.geoWind(gwarray[i, 2])

    # populate with geostrophic wind values (calculated analytically)
    for i in range(N + 1):
        gwarray[i, 4] = gp.uExact(gwarray[i, 0])

    # Output the array
    return gwarray


# Function to create an array with errors of geoWind_array(N)
def gwaerror(N):
    '''Creates an array that displays the error between the numerical
     and the analytic solution of the geostrophic wind, using
    geoWind_array(N).'''

    # Set N to integer
    N = int(N)

    # Initialize arrays
    e = np.zeros(N+1)
    d = geoWind_array(N)

    # Populate with errors of geoWind_array
    for i in range(N+1):
        e[i] = d[i, 4]-d[i, 3]

    return e


print(gwaerror(2))
