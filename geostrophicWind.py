# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 14:36:06 2019

@author: xd800637
"""

# MTMW12 assignment 3 for Hilary Weller, 15 October 2019.
# Python code to numerically differentiate the pressure in order to calculate
# the geostrophic wind relation using 2-point differencing, compare with the
# analytic solution and plot.

# import numpy as np
import matplotlib.pyplot as plt
import differentiate as d


# Does geoWind() give us a sensible answer for N = 10?
# print(d.geoWind_array(10))


# Does dta(N) give us the right answer for a small N?
assert d.dta(2) == 5e+5, 'Unexpected output from dta(N)'


# Graph of the numerical and analytical solutions for N = 10
# Format data
z = d.geoWind_array(10)
x = z[:, 0]
y1 = z[:, 3]
y2 = z[:, 4]
y3 = d.gwaerror(10)

# Create the plot
fig, ax1 = plt.subplots()


# instantiate a second axes that shares the same x-axis
ax2 = ax1.twinx()


# Format the solutions graph
color = 'tab:red'
ax2.set_ylabel('geostrophic wind', color=color)
ax1.set_xlabel('Location, with 11 evenly-spaced locations in total.')
ax2.plot(x, y1, color=color, zorder=2, marker='o', label='Numerical')
ax2.plot(x, y2, color='tab:green', zorder=3, marker='.', dashes=[3, 3],
         label='Analytical')
ax2.tick_params(axis='y', labelcolor=color)
ax2.grid(axis='y', color='tab:red')
ax2.set_ylim(0, 3)


# Format the error graph
color = 'darkorchid'
ax1.plot(x, y3, marker='H', markersize=8, zorder=1, color=color, label='Error')
ax1.set_ylabel('Error of numerical solution', color=color)
ax1.tick_params(axis='y', labelcolor=color)
ax1.set_ylim(-0.2, 0.1)


# Add legend
fig.legend(loc='centre')


# Keep appearances up.
fig.tight_layout()


# Display graphs
plt.show()
