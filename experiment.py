# A function for calculating the accuracy of the numerical methods implemented
# in differentiate.py. The output of the function is then displayed on a plot.


import differentiate as df
import numpy as np
import matplotlib.pyplot as plt


# The below function is far from optimal: complete arrays are calculated using
# gwaerror(), when only two lines of these arrays are required.
def accutest():
    '''Creates an array which displays the errors at y_0 and y_1 as calculated
    in geoWind_array().'''

    # Initialize array
    array = np.zeros(shape=(5, 4))

    # Populate with N values
    for i in range(5):
        array[i, 0] = 2**(i + 1)

    # Populate with dta(y)
    for i in range(5):
        array[i, 1] = df.dta(array[i, 0])

    # Populate with abs(error) at y_0
    for i in range(5):
        array[i, 2] = abs(df.gwaerror(array[i, 0])[0])

    # Populate with abs(error) at y_1
    for i in range(5):
        array[i, 3] = abs(df.gwaerror(array[i, 0])[1])

    return array


# Create plot of results
# Input data
a = accutest()
x = a[:, 1]
# x1 = np.linspace(np.exp(3.125e+4), np.exp(5e+5), 1000)
y1 = a[:, 2]
y2 = a[:, 3]

# Create the plot
fig, ax = plt.subplots()
ax.plot(x, y1, color='tab:red', label='y0')
ax.plot(x, y2, color='tab:blue', label='y1')
# ax.plot(np.log(x1), 2*np.log(x1))
# ax.plot(np.log(x1), np.log(x1))

# Format the plot
ax.set_xlabel('')
fig.tight_layout()
ax.grid()
ax.set_yscale('log')
ax.set_xscale('log')
plt.set_aspect('equal')

# Add legend
fig.legend(loc='centre')

# Display plot
plt.show()
