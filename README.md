# MTMW12_assig3

**geoParameters.py** sets the value of parameters for calculating the geostrophic 
wind. It also contains uExact, an analytic calculation of the geostrophic wind.

**differentiate.py** defines functions for a numerical solution of the geostrophic 
wind, and a function for analysing the error between the numerical and 
analytical solution.

**geostrophicWind.py** uses the functions defined in differentiate to generate a 
graph displaying the analytic and numerical solutions for 11 evenly spaced 
locations between our maximum values. The graph also displays the errors 
between these two solutions.

**experiment.py** defines a function to test the accuracy of the python 
implementation of the numerical methods featured in differentiate.py.

