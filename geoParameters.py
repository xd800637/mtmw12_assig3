# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 14:16:57 2019

@author: xd800637
"""
import numpy as np


# Input parameters for calculating the geostrophic wind.

pa = 1e5                # the mean pressure
pb = 200                # the magnitude of the pressure variations
f = 1e-4                # the Coriolis parameter
rho = 1.                # density
L = 2.4e6               # length scale of the pressure variations
ymin = 0.0              # minimum space dimension
ymax = 1e6              # maximum space dimension (k)

def pressure(y):
    '''The pressure at given location y'''
    return pa + pb*np.cos(y*np.pi/L)

def uExact(y):
    '''The gostrophic wind at given locations, y. Calculated analytically.'''
    return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)

def geoWind(dpdy):
    '''The geostrophic wind as a function of pressure gradient'''
    return -dpdy/(rho*f)
